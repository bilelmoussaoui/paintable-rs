use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use std::env::args;
mod qr_code;
mod qrcode_paintable;
use qr_code::QRCode;
use qrcode_paintable::QRCodePaintable;
mod imp {
    use super::*;
    use glib::subclass;

    #[derive(Debug)]
    pub struct Window {
        pub qrcode_paintable: QRCodePaintable,
    }

    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = gtk::ApplicationWindow;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn new() -> Self {
            Self {
                qrcode_paintable: QRCodePaintable::new(),
            }
        }
    }

    impl ObjectImpl for Window {
        fn constructed(&self, obj: &Self::Type) {
            obj.setup_widgets();
            self.parent_constructed(obj);
        }
    }
    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>) @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow;
}

impl Window {
    pub fn new<P: glib::IsA<gtk::Application>>(app: &P) -> Self {
        glib::Object::new(&[("application", app)]).expect("Failed to create Window")
    }

    pub fn regen_qrcode(&self, for_content: &str) {
        let self_ = imp::Window::from_instance(self);

        let qrcode = QRCode::from_string(for_content);
        self_.qrcode_paintable.set_qrcode(qrcode);
    }

    pub fn setup_widgets(&self) {
        let self_ = imp::Window::from_instance(self);
        self.set_title(Some("QR Code Generator"));
        self.set_default_size(500, 500);

        let image = gtk::Picture::new();
        image.set_halign(gtk::Align::Center);
        image.set_size_request(200, 200);
        image.set_paintable(Some(&self_.qrcode_paintable));

        let box_ = gtk::BoxBuilder::new()
            .orientation(gtk::Orientation::Vertical)
            .halign(gtk::Align::Center)
            .valign(gtk::Align::Center)
            .vexpand(true)
            .margin_top(48)
            .spacing(24)
            .build();

        let text_entry = gtk::EntryBuilder::new()
            .enable_emoji_completion(true)
            .show_emoji_icon(true)
            .build();
            
        text_entry.connect_changed(glib::clone!(@weak self as win => move |entry| {
            let content = entry.get_text().unwrap();
            win.regen_qrcode(&content);
        }));

        box_.append(&text_entry);
        box_.append(&image);
        self.set_child(Some(&box_));
    }
}

fn main() {
    let application = gtk::Application::new(
        Some("com.github.gtk-rs.examples.paintable"),
        Default::default(),
    )
    .expect("Initialization failed...");

    application.connect_activate(|app| {
        let win = Window::new(app);
        win.show();
    });

    application.run(&args().collect::<Vec<_>>());
}
